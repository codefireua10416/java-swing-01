/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaframes;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author human
 */
public class MainFrame extends JFrame implements ActionListener {
    
    private JButton button;
    private JButton button2;
    private JButton button3;

    public MainFrame() {
        initComponents();
    }

    private void initComponents() {
        setSize(480, 320);
        
        setTitle("Main frame");
//        setLocation(300, 300);
        // HOOK
//        setLocationRelativeTo(null);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        button = new JButton("Click ME!");
        button.addActionListener(this);
        button2 = new JButton("Click ME!");
        button2.addActionListener(this);
        button3 = new JButton("Click ME!");
        button3.addActionListener(this);
        
        JPanel panel = new JPanel();
        panel.add(button);
        panel.add(button2);
        panel.add(button3);
        
        Container container = getContentPane();
        container.add(panel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        MainFrame child = new MainFrame();
        child.setTitle("Child");
        child.setDefaultCloseOperation(MainFrame.DISPOSE_ON_CLOSE);
        child.setLocationRelativeTo((JButton)e.getSource());
        child.setVisible(true);
    }

}
